
# toutiaoDotIoFavBackup

1. How to build
clone this repo and go to the repo dir.
run 
```bash
go build .
```

2. Configuration

firt ,make a copy of config.sample.ini :
```bash
cp ./config.sample.ini config.ini
```

then edit config.ini use your favorite editor.

sample config:
```ini
[toutiao]
cookie = cookie is required!
user_agent = Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.75 Safari/537.36
start_page = 1
per_page = 100
sleep = 800
star_only = true

#mongodb info is only required when star_only = false
[mongo]
user = huangyewudeng
passwd = passw0rd
host = 127.0.0.1
port = 27017
db_name = github
collection = starred
```

3. run it

```bash
./toutiaoDotIoFavBackup
```